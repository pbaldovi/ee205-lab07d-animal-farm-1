///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file config.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   08 Mar 2022
//////////////////////////////////////////////////////////////////////////////

#define PROGRAM_NAME "Animal Farm 1"
