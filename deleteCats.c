///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file deleteCats.c
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   08 Mar 2022
//////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include "catDatabase.h"
#include "deleteCats.h"

void deleteAllCats() {
   memset(cats, 0, sizeof(cats));
   currentCat = 0;
}

void deleteCat(int index) {
   if (validateIndex(index) == 0) {    
      for (int i = index; i < MAX_CATS - 1; i++) {
         strcpy(cats[i].names, cats[i + 1].names);
         cats[i].genders = cats[i + 1].genders;
         cats[i].breeds = cats[i + 1].breeds;
         cats[i].isFixedArr = cats[i + 1].isFixedArr;
         cats[i].weights = cats[i + 1].weights;
   
      }
   }
}
