///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file catDatabase.c
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   08 Mar 2022
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "catDatabase.h"
#include "config.h"

int currentCat = 0;

struct Cat cats[MAX_CATS];

int validateIndex(int i) {
   if (i < 0 || i > MAX_CATS - 1) {
      fprintf(stderr, "%s: Index out of bounds.\n", PROGRAM_NAME);
      return 1;
   }
   return 0;
}
