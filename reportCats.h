///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file reportCats.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   08 Mar 2022
//////////////////////////////////////////////////////////////////////////////

#pragma once

void printCat(int index);
void printAllCats();
int findCat(const char name[]);
extern char* genderName(const enum Gender gender);
extern char* breedName(const enum Breed breed);
extern char* colorName (const enum Color color);
