///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file catDatabase.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   08 Mar 2022
//////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>

#define MAX_NAME_LENGTH (50)
#define MAX_CATS        (1024)

enum Gender {
   UNKNOWN_GENDER, 
   MALE, 
   FEMALE
};
enum Breed {
   UNKNOWN_BREED, 
   MAINE_COON, 
   MANX, 
   SHORTHAIR, 
   PERSIAN, 
   SPHYNX
};
enum Color {
   BLACK,
   WHITE,
   RED,
   BLUE,
   GREEN,
   PINK
};

extern int currentCat;

struct Cat {
 char names[MAX_NAME_LENGTH];
 enum Gender genders;
 enum Breed breeds;
 bool isFixedArr;
 float weights;
 enum Color collarColor1;
 enum Color collarColor2;
 unsigned long long license;
};

extern struct Cat cats[MAX_CATS];

extern int validateIndex(int i);

void initializeDatabase();
