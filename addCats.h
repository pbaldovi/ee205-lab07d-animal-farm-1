///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file addCats.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   08 Mar 2022
//////////////////////////////////////////////////////////////////////////////

#pragma once
#include <stdbool.h>

extern int addCat(   const char name[], 
                     const enum Gender gender, 
                     const enum Breed breed, 
                     const bool isFixed, 
                     const float weight, 
                     const enum Color collar1,
                     const enum Color collar2,
                     const unsigned long long licenseNum);
