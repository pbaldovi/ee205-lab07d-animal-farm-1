///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file updateCats.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   08 Mar 2022
//////////////////////////////////////////////////////////////////////////////

#pragma once

extern int updateCatName(int index, char newName[]);
extern void fixCat(int index);
extern void updateCatWeight(int index, float newWeight);
extern void updateCatCollar1(int index, const enum Color color);
extern void updateCatCollar2(int index, const enum Color color);
extern void updateLicense(int index, unsigned long long licenseNum);
