///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
//
// @file deleteCats.h
//
// @author Paulo Baldovi <pbaldovi@hawaii.edu>
// @date   08 Mar 2022
//////////////////////////////////////////////////////////////////////////////

#pragma once

void deleteAllCats();
void deleteCat(int index);
